const express = require("express");
const makeStoppable = require("stoppable")
const http = require("http");
const path = require("path");
const bodyParser = require("body-parser");

const indexRouter = require("./routes/index");
const doneRouter = require("./routes/done");
const apiRouter = require("./routes/api");

const app = express();
app.use(express.static(path.join(__dirname, "../static")));

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "../views"));


const server = makeStoppable(http.createServer(app));

app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", indexRouter);
app.use("/done", doneRouter);
app.use("/api", apiRouter);


module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
