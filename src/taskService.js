const Task = require("./task");
const dbForSaveTasks = [];

const getAllTasks = async () => dbForSaveTasks;

const addTasks = async (taskName) => dbForSaveTasks.push(new Task(taskName));

const hasTask = async (taskName) => dbForSaveTasks.some((task) => task.name === taskName && !task.isCompleted()); 

const getTask = async (taskName) => dbForSaveTasks.find((task) => task.name === taskName && !task.isCompleted());

module.exports = {
    getAllTasks,
    addTasks, 
    hasTask, 
    getTask
}