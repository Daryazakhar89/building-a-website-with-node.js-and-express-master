const { check } = require("express-validator");
const taskService = require("../taskService");

const validateTaskName = [
    check("task")
        .trim()
        .isLength({ min: 3 })
        .escape()
        .withMessage("Minimal length for task name is 3 letter!"),
    check("task").custom(async (taskName, { req }) => {
        const isTaskNameAlreadyExists = await taskService.hasTask(taskName);

        if (isTaskNameAlreadyExists) {
        throw new Error(`Task ${taskName} already exist`);
        }
    }),
];

module.exports = {
    validateTaskName
}

