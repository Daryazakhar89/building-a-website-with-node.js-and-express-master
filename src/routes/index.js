const session = require('cookie-session')
const router = require("express").Router();

const { validateTaskName } = require("./validations");
const { handleGetTodoTasks, handleTaskCreate } = require("./handlers");

router.use(
  session({
    name: 'session',
    keys: ['Gjjkll8999', 'ejjJJKKKKH8494'],
  })
);

router.get("/", handleGetTodoTasks);

router.post("/", validateTaskName, handleTaskCreate);

module.exports = router;
