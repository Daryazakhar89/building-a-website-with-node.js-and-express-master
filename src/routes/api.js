const { request, response } = require("express");
const express = require("express");

const router = express.Router();

const { handleSetTaskDone } = require("./handlers");

router.post("/tasks/:taskName/done", handleSetTaskDone);

module.exports = router;

