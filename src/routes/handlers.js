const taskService = require("../taskService");
const { validationResult } = require("express-validator");

const handleSetTaskDone = async (request, response) => {
    const { taskName } = request.params;
    const task = await taskService.getTask(taskName);
    task.setCompleted();

    response.redirect("/");
}

const handleGetDoneTasks = async (request, response) => {
    response.render("layout", {
        pageTitle: "Simple TODO Application",
        listHeader: "Done list",
        template: "done",
        tasks: (await taskService.getAllTasks())
        .filter((task) => task.isCompleted())
        .slice()
        .sort((a, b) => b.completionDate - a.completionDate)
    });
}

const handleGetTodoTasks = async (request, response) => {
    tasks = await taskService.getAllTasks();

    let errors = [];

    if (request.session.form) {
        errors = request.session.form.errors;
        request.session.form.errors = [];
    }

    response.render("layout", {
        pageTitle: "Simple TODO Application",
        listHeader: "Todo list",
        template: "index",
        errors,
        tasks: (await taskService.getAllTasks())
        .filter((task) => !task.isCompleted())
        .slice()
        .reverse(),
    });
}

const handleTaskCreate = (request, response) => {
    const taskName = request.body.task;
    const errors = validationResult(request);

    if (!errors.array().length) {
        taskService.addTasks(taskName);
    }

    request.session.form = {
        errors: errors.array()
    };

    return response.redirect("/");
};



module.exports = {
    handleSetTaskDone,
    handleGetDoneTasks,
    handleGetTodoTasks,
    handleTaskCreate
}