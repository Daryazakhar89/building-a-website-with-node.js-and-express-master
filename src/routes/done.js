const { response } = require("express");
const express = require("express");

const { handleGetDoneTasks } = require("./handlers");

const router = express.Router();

router.get("/", handleGetDoneTasks);

module.exports = router;
