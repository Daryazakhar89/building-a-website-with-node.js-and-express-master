module.exports = class Task {
    constructor(name) {
        this.name = name;
        this.status = "inProgress";
        this.completionDate = null;
    }

    setCompleted() {
        this.status = "completed";
        this.completionDate = new Date();
    }

    isCompleted() {
        return this.status === "completed";
    }
}